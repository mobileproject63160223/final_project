import 'dart:ffi';
import 'dart:io';
import 'Obj.dart';

class Tbl {
  int count = 0;
  //Table size
  late int width;
  late int heigh;
  //Tree1
  late int xt1;
  late int yt1;
  late String st1;
  //Tree2
  late int xt2;
  late int yt2;
  late String st2;
  //Tree3
  late int xt3;
  late int yt3;
  late String st3;
  //Hole1
  late int xh1;
  late int yh1;
  late String sh1;
  //Hole2
  late int xh2;
  late int yh2;
  late String sh2;
  //Home
  late int xhome;
  late int yhome;
  late String shome;
  //Player
  String? wayplayer;
  late int xplayer;
  late int yplayer;
  String splayer = "P";
  //Dead
  String? dead;

  late var growableList = [];
  String showTbl() {
    for (int o = 0; o < width * heigh; o++) {
      growableList.add("-");
    }
    print("------------------------------");
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < heigh; j++) {
        if (i == xplayer && j == yplayer) {
          // Player
          stdout.write("| ");
          stdout.write(splayer);
          stdout.write(" |");
        } else if (i == xt1 - 1 && j == yt1 - 1) {
          // Tree1
          stdout.write("| ");
          stdout.write(st1);
          stdout.write(" |");
        } else if (i == xt2 - 1 && j == yt2 - 1) {
          // Tree2
          stdout.write("| ");
          stdout.write(st2);
          stdout.write(" |");
        } else if (i == xt3 - 1 && j == yt3 - 1) {
          // Tree3
          stdout.write("| ");
          stdout.write(st3);
          stdout.write(" |");
        } else if (i == xh1 - 1 && j == yh1 - 1) {
          // Hole1
          stdout.write("| ");
          stdout.write(sh1);
          stdout.write(" |");
        } else if (i == xh2 - 1 && j == yh2 - 1) {
          // Hole2
          stdout.write("| ");
          stdout.write(sh2);
          stdout.write(" |");
        } else if (i == xhome - 1 && j == yhome - 1) {
          // Hole2
          stdout.write("| ");
          stdout.write(shome);
          stdout.write(" |");
        } else {
          stdout.write("| ");
          stdout.write(growableList[count]);
          stdout.write(" |");
        }
        count++;
      }
      print("");
    }
    print("------------------------------");
    /*stdout.write("------------------------------------------\n");
  stdout.write(growableList[1]);*/
    return "";
  }
}
