import 'dart:ffi';
import 'dart:io';

class Obj {
  late int x;
  late int y;
  late String symbol;
}

class Tree1 extends Obj {
  int x = 3;
  int y = 4;
  String symbol = "T";

  String showT1(int x, int y, String symbol) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
    return "";
  }
}

class Tree2 extends Obj {
  int x = 7;
  int y = 2;
  String symbol = "T";

  String showT2(int x, int y, String symbol) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
    return "";
  }
}

class Tree3 extends Obj {
  int x = 6;
  int y = 5;
  String symbol = "T";

  String showT3(int x, int y, String symbol) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
    return "";
  }
}

class Hole1 extends Obj {
  int x = 5;
  int y = 5;
  String symbol = "-";

  String showH1(int x, int y, String symbol) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
    return "";
  }
}

class Hole2 extends Obj {
  int x = 3;
  int y = 2;
  String symbol = "-";

  String showH2(int x, int y, String symbol) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
    return "";
  }
}

class Home extends Obj {
  int x = 7;
  int y = 6;
  String symbol = "♕";

  String showHome(int x, int y, String symbol) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
    return "";
  }
}

class Player extends Obj {
  String way = "";
  String symbol = "P";

  String showP1(var way, String symbol) {
    this.way = way;
    this.symbol = symbol;
    return "";
  }
}
