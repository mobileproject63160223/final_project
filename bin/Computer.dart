import 'dart:io';

import 'Obj.dart';
import 'Tbl.dart';

void main() {
  //Table
  Tbl tb = Tbl();
  tb.width = 7;
  tb.heigh = 6;
  //Tree1
  Tree1 t1 = Tree1();
  tb.xt1 = t1.x;
  tb.yt1 = t1.y;
  tb.st1 = t1.symbol;
  //Tree2
  Tree2 t2 = Tree2();
  tb.xt2 = t2.x;
  tb.yt2 = t2.y;
  tb.st2 = t2.symbol;
  //Tree3
  Tree3 t3 = Tree3();
  tb.xt3 = t3.x;
  tb.yt3 = t3.y;
  tb.st3 = t3.symbol;
  //Hole1
  Hole1 h1 = Hole1();
  tb.xh1 = h1.x;
  tb.yh1 = h1.y;
  tb.sh1 = h1.symbol;
  //Hole2
  Hole2 h2 = Hole2();
  tb.xh2 = h2.x;
  tb.yh2 = h2.y;
  tb.sh2 = h2.symbol;
  //Home
  Home home = Home();
  tb.xhome = home.x;
  tb.yhome = home.y;
  tb.shome = home.symbol;
  //Show table & rule
  print("กฏของเกมส์ Gogogohome");
  print("1. เกมส์นี้จะมีผู้เล่น 1คน โดยผู้เล่นจะมีสัญลักษณ์ในเกมส์ คือ P");
  print(
      "2. ปุ่มที่ใช้ควบคุมผู้เล่น w = ขึ้นข้างบน a = ไปทางซ้าย s = ลงข้างล่าง d = ไปทางขวา");
  print(
      "3. สิ่งกีดขวางในเกมส์(ปัจจุบันมี 2อย่าง)ได้แก่ ต้นไม้ (T) และ หลุมดำ( )");
  print("4. เมื่อผู้เล่นเดินชนต้นไม้ ผู้เล่นจะกลับไปยังจุดเริ่มต้นเสมอ");
  print("5. เมื่อผู้เล่นตกหลุมดำ เกมส์จะจบ และจะพ่ายแพ้");
  print("ขอให้สนุกกับเกมส์ GogogoHome - Demo ของเรา !!!!");
  //print(tb.showTbl());
  //Player
  Player player = Player();
  int i;
  int walk = 20;
  int swalk = 0;
  int stamina = 20;
  tb.xplayer = 0;
  tb.yplayer = 0;
  var way;
  print(tb.showTbl());
  for (i = 0; i < walk + swalk; i++) {
    stdout.write("Please input your way : ");
    way = stdin.readLineSync();
    player.way = way;

    if (tb.yplayer == 5 && player.way == "d") {
      tb.wayplayer = player.way;
      tb.xplayer = tb.xplayer + 0;
      tb.yplayer = tb.yplayer + 0;
      print("");
      print(tb.showTbl());
      print("กำแพงสูงจัง !! เราไปทางอื่นกันเถอะ");
      stamina = stamina - 0;
      swalk = swalk + 1;
    }
    if (player.way == "d" && tb.yplayer != 5) {
      tb.wayplayer = player.way;
      tb.xplayer = tb.xplayer + 0;
      tb.yplayer = tb.yplayer + 1;
      print("");
      print(tb.showTbl());
      stamina = stamina - 1;
    }
    if (tb.xplayer == 6 && player.way == "s") {
      tb.wayplayer = player.way;
      tb.xplayer = tb.xplayer + 0;
      tb.yplayer = tb.yplayer + 0;
      print("");
      print(tb.showTbl());
      print("กำแพงสูงจัง !! เราไปทางอื่นกันเถอะ");
      stamina = stamina - 0;
      swalk = swalk + 1;
    }
    if (player.way == "s" && tb.xplayer != 6) {
      tb.wayplayer = player.way;
      tb.xplayer = tb.xplayer + 1;
      tb.yplayer = tb.yplayer + 0;
      print("");
      print(tb.showTbl());
      stamina = stamina - 1;
    }
    if (tb.yplayer == 0 && player.way == "a") {
      tb.wayplayer = player.way;
      tb.xplayer = tb.xplayer + 0;
      tb.yplayer = tb.yplayer + 0;
      print("");
      print(tb.showTbl());
      print("กำแพงสูงจัง !! เราไปทางอื่นกันเถอะ");
      stamina = stamina - 0;
      swalk = swalk + 1;
    }
    if (player.way == "a" && tb.yplayer != 0) {
      tb.wayplayer = player.way;
      tb.xplayer = tb.xplayer + 0;
      tb.yplayer = tb.yplayer - 1;
      print("");
      print(tb.showTbl());
      stamina = stamina - 1;
    }
    if (tb.xplayer == 0 && player.way == "w") {
      tb.wayplayer = player.way;
      tb.xplayer = tb.xplayer + 0;
      tb.yplayer = tb.yplayer + 0;
      print("");
      print(tb.showTbl());
      print("กำแพงสูงจัง !! เราไปทางอื่นกันเถอะ");
      stamina = stamina - 0;
      swalk = swalk + 1;
    }
    if (player.way == "w" && tb.xplayer != 0) {
      tb.wayplayer = player.way;
      tb.xplayer = tb.xplayer - 1;
      tb.yplayer = tb.yplayer + 0;
      print("");
      print(tb.showTbl());
      stamina = stamina - 1;
    }
    if (tb.xhome - 1 == tb.xplayer && tb.yhome - 1 == tb.yplayer) {
      print("");
      print(tb.showTbl());
      print("Stamina คงเหลือ : $stamina");
      print(">>> W I N <<< - ขอให้คุณกลับบ้านอย่างมีความสุข");
      stamina = stamina - 1;
      break;
    }
    if (tb.xh1 - 1 == tb.xplayer && tb.yh1 - 1 == tb.yplayer) {
      tb.splayer = "X";
      print("");
      print(tb.showTbl());
      stamina = 0;
      print("Stamina คงเหลือ : $stamina");
      print(
          ">>> G A M E O V E R <<< - ขอให้คุณไปสู่ภพภูมิที่ดี - เนื่องจากคุณตกหลุมพราง");
      break;
    }
    if (tb.xh2 - 1 == tb.xplayer && tb.yh2 - 1 == tb.yplayer) {
      tb.splayer = "X";
      print("");
      print(tb.showTbl());
      stamina = 0;
      print("Stamina คงเหลือ : $stamina");
      print(
          ">>> G A M E O V E R <<< - ขอให้คุณไปสู่ภพภูมิที่ดี - เนื่องจากคุณตกหลุมพราง");
      break;
    }
    if (tb.xt1 - 1 == tb.xplayer && tb.yt1 - 1 == tb.yplayer) {
      tb.xplayer = 0;
      tb.yplayer = 0;
      print("");
      print(tb.showTbl());
      stamina - 1;
      print(">>> คุณชนเข้ากับต้นไม้ <<< - ดังนั้นคุณต้องเริ่มใหม่ตั้งแต่ต้น");
    }
    if (tb.xt2 - 1 == tb.xplayer && tb.yt2 - 1 == tb.yplayer) {
      tb.xplayer = 0;
      tb.yplayer = 0;
      print("");
      print(tb.showTbl());
      stamina - 1;
      print(">>> คุณชนเข้ากับต้นไม้ <<< - ดังนั้นคุณต้องเริ่มใหม่ตั้งแต่ต้น");
    }
    if (tb.xt3 - 1 == tb.xplayer && tb.yt3 - 1 == tb.yplayer) {
      tb.xplayer = 0;
      tb.yplayer = 0;
      print("");
      print(tb.showTbl());
      stamina - 1;
      print(">>> คุณชนเข้ากับต้นไม้ <<< - ดังนั้นคุณต้องเริ่มใหม่ตั้งแต่ต้น");
    }
    print("Stamina คงเหลือ : $stamina");
  }
}
